﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_Practice
{

    public class Stage
    {
        public class Stage2
        {

        }
    }

    public class Actor
    {
        protected int HP = 100;
        public float Defence = 1f;
        public float MagDef = 1f;

      
        public void SetDamage(int p_damage)
        {
            HP = HP - (int)((float)10 / Defence);
            if (HP < 0)
            {
                HP = 0;
            }
        }
    }

    public class Player : Actor
    {
       
    }

    public class Monster : Actor
    {
        
    }

    class InGame
    {
        Player playcls = new Player();
        Monster mobcls = new Monster();

        bool ISLoop()
        {
             ConsoleKeyInfo info = Console.ReadKey();
            if(info.Key == ConsoleKey.Q)
            {
                return false;
            }

            return true;
        }
        Random rand = new System.Random();
        void InGameLogic()
        {

            int result = rand.Next() % 2;

            if( result == 0)
            {
                playcls.SetDamage(10);
           
            }
            else
            {
                mobcls.SetDamage(10);
            }
        }
        public void GameStart()
        {
            InitSetting();

            bool isloop = false;
            while (true)
            {
                //게임 로직 쪽

                isloop = ISLoop();
                if (isloop == false)
                {
                    break;
                }
            }
            Release();
        }

        void Release()
        {
            playcls = null;
            mobcls = null;
        }
        public void InitSetting()
        {
          
        }
    }
    class Program
    { 
     
        static void Main(string[] args)
        {
            InGame gamecls = new InGame();
            gamecls.GameStart();


        }
    }
}
